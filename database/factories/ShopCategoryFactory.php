<?php

use Faker\Generator as Faker;
use App\ShopCategory;

$factory->define(App\ShopCategory::class, function (Faker $faker) {

    //ShopCategory::
    return [
        'name' => $faker->sentence(2),
        'parent_id' => 0

    ];
});
