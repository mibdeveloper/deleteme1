<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopCategory extends Model
{
    public $fillable = ['name', 'parent_id'];

    public function subcategories() {
        return $this->hasMany('App\ShopCategory', 'parent_id', 'id');
    }



}
