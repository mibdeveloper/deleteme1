<ul>
    @foreach($childs as $child)
    <li>
        {!!$child->name!!}
        @can('add', app($dataType->model_name))
        <a href="{{ route('voyager.'.$dataType->slug.'.create-category', ['id' => $child->id]) }}" class="btn btn-success btn-add-new">
            <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
        </a>
        @endcan

        @can('delete', app($dataType->model_name))
        <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-danger delete">
            <i class="voyager-trash"></i> <span>{{ __('voyager::generic.delete') }}</span>
        </a>
        @endcan


        @if(count($child->subcategories))
        @include('voyager::shop-categories.subcategory',
        ['childs'=>$child->subcategories])
        @endif
    </li>
    @endforeach
</ul>
